package Tetris.view;

import Tetris.animation.AnimationApplet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import Tetris.model.GameState;
import Tetris.model.Pair;
import Tetris.model.Tetromino;
import Tetris.model.Tetromino.TetrominoEnum;
import Tetris.view.NetworkDialog;

public class TetrisUI extends AnimationApplet implements Observer {

	private static final long serialVersionUID = 1L;
	private GameState gameState;
	private boolean isPaused = true;
	public static final Color PANEL_BG_COLOR = new Color(66, 103, 178);
	public static final Color BOARD_BG_COLOR = new Color(29, 42, 72);
	private KeyListener klController = null;
	private ActionListener alController = null;
	private NetworkDialog netDialog = null;

	public TetrisUI(String[] args) {
		super(args);
		this.netDialog = new NetworkDialog();
	}

	@Override
	public void init() {
		this.delay = 500;
		super.init();
	}

	@Override
	public void start() {
		super.start();
		repaint();
		this.isPaused = false;
	}

	@Override
	public void stop() {
		super.stop();
		this.isPaused = true;
	}

	public void resetTimer() {
		this.timer = new Timer(this.delay, e -> periodicTask());
	}

	@Override
	public void periodicTask() {
		if (this.gameState.isGameActive() || this.gameState.isGameOver()) {
			this.gameState.advanceTetromino();
		}
		super.periodicTask();
	}

	public boolean isPaused() {
		return this.isPaused;
	}

	public void setModel(GameState gameState) {
		this.gameState = gameState;
	}

	public void setActionListener(ActionListener controller) {
		this.alController = controller;
	}

	public void setKeyListener(KeyListener controller) {
		this.klController = controller;
	}

	public void setStatusBarText(String text) {
		this.statusBar.setText(text);
	}

	public NetworkDialog getNetworkDialog() {
		return this.netDialog;
	}

	@Override
	protected JPanel createUI() {
		statusBar.setOpaque(true);
		statusBar.setBackground(BOARD_BG_COLOR);
		statusBar.setText("");
		statusBar.setFont(new Font(statusBar.getName(), 20, 20));
		statusBar.setForeground(Color.WHITE);
		JPanel root = new JPanel();
		root.setLayout(new BorderLayout());
		root.add(createToolBar(), "West");
		root.add(createMenuBar(), "North");

		root.add(this, "Center");
		root.add(statusBar, "South");

		return root;
	}

	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);

		JMenu optionsMenu = new JMenu("Game Menu");
		ImageIcon menuIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/Menu.png"));
		optionsMenu.setIcon(menuIcon);
		optionsMenu.setMnemonic(KeyEvent.VK_M);

		JMenuItem newGameMenu = new JMenuItem("New Game");
		ImageIcon newGameIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/NewGame.png"));
		newGameMenu.setIcon(newGameIcon);
		KeyStroke ctrlMkeyStroke = KeyStroke.getKeyStroke("control N");
		newGameMenu.setAccelerator(ctrlMkeyStroke);
		newGameMenu.addActionListener(this.alController);

		JMenuItem instructionsMenu = new JMenuItem("Control Keys");
		ImageIcon controlsIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/Controls.png"));
		instructionsMenu.setIcon(controlsIcon);
		KeyStroke ctrlCkeyStroke = KeyStroke.getKeyStroke("control C");
		instructionsMenu.setAccelerator(ctrlCkeyStroke);
		instructionsMenu.addActionListener(this::controlMenu);

		JMenuItem quitMenu = new JMenuItem("Quit");
		ImageIcon exitIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/Exit.png"));
		quitMenu.setIcon(exitIcon);
		KeyStroke ctrlEkeyStroke = KeyStroke.getKeyStroke("control E");
		quitMenu.setAccelerator(ctrlEkeyStroke);
		quitMenu.addActionListener(this::quitGames);

		optionsMenu.add(newGameMenu);
		optionsMenu.add(instructionsMenu);
		optionsMenu.addSeparator();
		optionsMenu.add(quitMenu);

		menuBar.add(optionsMenu);
		return menuBar;
	}

	private void controlMenu(ActionEvent event) {

		TetrisUI.this.stop();
		JDialog instructionsDialog = new JDialog();

		instructionsDialog.setSize(350, 175);
		instructionsDialog.setTitle("Controls");

		StringBuilder sb = new StringBuilder();
		sb.append("Right Arrow Key: Move tetromino to the right.\n");
		sb.append("Left Arrow Key: Move tetromino to the left.\n");
		sb.append("Down Arrow Key: Push tetromino down.\n");
		sb.append("\"C\" Key: Rotate tetromino right.\n");
		sb.append("\"Z\" Key: Rotate tetromino left.\n");
		sb.append("Escape Key: Pause-Start game.\n");
		JTextArea instructionsText = new JTextArea(sb.toString());

		instructionsText.setEditable(false);
		instructionsText.setBackground(Color.WHITE);
		instructionsText.setForeground(Color.BLACK);
		instructionsText.setFont(new Font(TetrisUI.this.statusBar.getName(), 16, 16));
		instructionsDialog.add(new JScrollPane(instructionsText));
		instructionsDialog.setVisible(true);

	}

	protected void quitGames(ActionEvent event) {
		System.exit(0);
	}

	protected JToolBar createToolBar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setOrientation(SwingConstants.VERTICAL);

		ImageIcon gameIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/NewGame.png"));
		JButton gameButton = new JButton(gameIcon);
		gameButton.setToolTipText("Starts a new game.");
		gameButton.addActionListener(this.alController);
		gameButton.setFocusable(false);
		toolBar.add(gameButton);

		ImageIcon controlIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/Controls.png"));
		JButton controlButton = new JButton(controlIcon);
		controlButton.setToolTipText("Displays game instructions.");
		controlButton.addActionListener(this::controlMenu);
		toolBar.add(controlButton);
		controlButton.setFocusable(false);

		ImageIcon exitIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/Exit.png"));
		JButton exitButton = new JButton(exitIcon);
		exitButton.setToolTipText("Exits the game");
		exitButton.addActionListener(this::quitGames);
		exitButton.setFocusable(false);
		toolBar.add(exitButton);
		return toolBar;
	}

	@Override
	protected void paintFrame(Graphics g) {
		if (g == null) {
			return;
		}
		syncPaintFrame(g);
	}

	private void syncPaintFrame(Graphics g) {
		g.setColor(PANEL_BG_COLOR);
		g.fillRect(0, 0, this.dim.width, this.dim.height);
		g.setColor(BOARD_BG_COLOR);
		g.fillRect(25, 25, 300, 600);

		if (this.gameState.isGameActive() || this.gameState.isGameOver()) {
			drawTetrominos(g, this.gameState.getBoard(), 30, 25, 25);
		}
		drawBoard(g, 30, 25, 25);
		drawStatus(g);
	}

	public void drawTetrominos(Graphics g, TetrominoEnum[][] board, int squareSize, int marginLeft, int marginTop) {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 20; y++) {

				if (board[y][x] != null) {

					g.setColor(getTetrominoColor(board[y][x]));
					g.fillRect(marginLeft + x * squareSize, marginTop + y * squareSize, squareSize, squareSize);
				}
			}
		}
	}

	public void drawBoard(Graphics g, int squareSize, int marginLeft, int marginTop) {
		g.setColor(Color.LIGHT_GRAY);
		for (int i = 0; i <= 10; i++) {
			g.drawLine(marginLeft + i * squareSize, marginTop, marginLeft + i * squareSize,
					marginTop + squareSize * 20);
		}

		for (int i = 0; i <= 20; i++) {
			g.drawLine(marginLeft, marginTop + i * squareSize, marginLeft + squareSize * 10,
					marginTop + i * squareSize);
		}
	}

	protected void drawStatus(Graphics g) {
		g.setColor(BOARD_BG_COLOR);
		for (int i = 0; i < 3; i++) {
			g.drawRect(350 + i, 25 + i, 180, 125);
		}
		if (this.gameState.isGameActive()) {

			Tetromino nextTetromino = this.gameState.getNextTetromino();
			if (nextTetromino == null) {
				return;
			}
			Iterator<Pair<Integer, Integer>> iter = nextTetromino.iterator();
			Color tetroColor = getTetrominoColor(nextTetromino.getTetrominoType());
			while (iter.hasNext()) {
				Pair<Integer, Integer> pair = (Pair) iter.next();
				g.setColor(tetroColor);
				g.fillRect(400 + (pair.getX()) * 25, 75 + (pair.getY()) * 25, 25, 25);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(400 + (pair.getX()) * 25, 75 + (pair.getY()) * 25, 25, 25);
			}
		}

		g.setColor(Color.WHITE);
		g.setFont(new Font(getFont().getName(), 24, 24));
		g.drawString("Next Figure", 360, 50);

		g.setColor(BOARD_BG_COLOR);
		for (int i = 0; i < 3; i++) {
			g.drawRect(350 + i, 175 + i, 185, 135);
		}
		g.setColor(Color.WHITE);
		g.drawString("Level: " + this.gameState.getLevel(), 360, 200);
		g.drawString("Lines: " + this.gameState.getLineCount(), 360, 250);
		g.drawString("Score: " + this.gameState.getScore(), 360, 300);

		Tetromino.TetrominoEnum[][] arrayOfTetrominoEnum = this.gameState.getOpponentsBoard();

		if (this.gameState.isMultiPlayerGame()) {

			g.setColor(BOARD_BG_COLOR);
			for (int i = 0; i < 3; i++) {
				g.drawRect(350 + i, 335 + i, 185, 290);
			}

			g.setColor(Color.WHITE);
			g.setFont(new Font(getFont().getName(), 14, 14));
			g.drawString("Name:" + this.gameState.getRemotePlayerName(), 360, 356);
			g.drawString("Score: " + this.gameState.getRemoteScore(), 440, 356);
			g.drawString("Status: ", 440, 381);
			
			if(this.gameState.isMultiPlayerGame() && this.gameState.isConnectionActive()) {
				g.setColor(Color.GREEN);
			}
			else {
				g.setColor(Color.RED);
			}
			g.fillOval(500,363,25,25);
			
			g.setColor(BOARD_BG_COLOR);
			g.fillRect(390, 415, 100, 200);

			if (arrayOfTetrominoEnum != null) {
				drawTetrominos(g, arrayOfTetrominoEnum, 10, 390, 415);
			}
			drawBoard(g, 10, 390, 415);
		}
	}

	private Color getTetrominoColor(Tetromino.TetrominoEnum tetrominoEnum) {
		switch (tetrominoEnum) {
		case I:
			return Color.RED;
		case J:
			return Color.GREEN;
		case L:
			return Color.PINK;
		case O:
			return Color.CYAN;
		case S:
			return Color.MAGENTA;
		case Z:
			return Color.YELLOW;
		case T:
			return Color.ORANGE;
		default:
			break;
		}
		return Color.WHITE;
	}

	@Override
	public KeyListener getKeyListener() {
		return this.klController;
	}

	@Override
	public void update(Observable obs, Object obj) {
		if (this.gameState.isGameOver()) {

			stop();
			this.delay = 500;
			resetTimer();

			this.statusBar.setText("Game Over!");
			if (this.gameState.isRemoteGameOver()) {
				if (this.gameState.getRemoteScore() < this.gameState.getScore()) {
					this.statusBar.setText("You Win!!!");
				} else if (this.gameState.getRemoteScore() == this.gameState.getScore()) {
					this.statusBar.setText("It's A Tie!!");
				} else {
					this.statusBar.setText("You Lose!!!");
				}
			}

			return;
		}

		if (this.gameState.levelUpgrade()) {

			stop();
			this.delay = (int) (this.delay * 0.8D);
			resetTimer();
			start();
		}
		repaint();
	}
}