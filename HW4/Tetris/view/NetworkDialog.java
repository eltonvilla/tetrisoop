package Tetris.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class NetworkDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    private JTextArea logTextArea;
    private final Dimension DIMENSION = new Dimension(500, 500);

    public JButton connectButton;

    public JButton hostButton;
    public JTextField hostnameTextField;
    public JTextField portTextField;
    private JPanel connectPanel;

    public void run() {
        configureGui();
        setSize(this.DIMENSION);
    }

    public void setActionListener(ActionListener alController) {
        this.hostButton.addActionListener(alController);
        this.connectButton.addActionListener(alController);
    }

    public void appendToLogTextArea(String logEntry) {
        this.logTextArea.append(logEntry);
        this.logTextArea.repaint();
    }

    private void configureGui() {
        this.connectPanel = new JPanel(new FlowLayout(3));
        this.connectButton = new JButton("Connect:");
        this.connectButton.setFocusPainted(false);
        this.hostButton = new JButton("Host Game");
        this.hostButton.setFocusPainted(false);
        this.hostnameTextField = new JTextField("localhost", 15);
        this.portTextField = new JTextField("4545", 5);
        this.connectPanel.add(this.hostButton);
        this.connectPanel.add(this.connectButton);
        this.connectPanel.add(this.hostnameTextField);
        this.connectPanel.add(this.portTextField);

        this.logTextArea = new JTextArea(10, 30);
        this.logTextArea.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(this.logTextArea);

        setLayout(new BorderLayout());
        add(this.connectPanel, "North");
        add(logScrollPane, "Center");
    }
}