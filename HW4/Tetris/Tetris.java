package Tetris;

import Tetris.controller.Controller;
import Tetris.model.GameState;
import Tetris.view.TetrisUI;

/* Team: Elton Villa and Alejandro Villarreal
*
*
*/

/**
 * Tetris class which sets up Tetris game
 */
public class Tetris {

	/**
	 * Main method creates GameState, Controller, and TetrisUI to function together
	 */
	public static void main(String[] args) {
		GameState gameState = new GameState();
		Controller controller = new Controller();
		controller.setModel(gameState);
		TetrisUI gui = new TetrisUI(args);
		gui.setActionListener(controller);
		gui.setKeyListener(controller);
		gui.getNetworkDialog().addWindowListener(controller);
		gui.getNetworkDialog().run();
		gui.getNetworkDialog().setActionListener(controller);
		gui.setModel(gameState);
		controller.setView(gui);
		gameState.addObserver(gui);

		gui.run();
	}
}