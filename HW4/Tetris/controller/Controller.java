package Tetris.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;
import javax.swing.JOptionPane;
import Tetris.model.GameState;
import Tetris.model.Tetromino;
import Tetris.model.Tetromino.TetrominoEnum;
import Tetris.view.TetrisUI;
import Tetris.model.NetworkAdapter;
import Tetris.model.NetworkMessageListener;

/**
 * Controller detects keys and clicks on the TetrisUI
 */
public class Controller implements KeyListener, ActionListener, NetworkMessageListener, WindowListener {

	private TetrisUI view;
	private GameState gameState;
	private Thread hostingThread = null;

	/**
	 * Set view
	 */
	public void setView(TetrisUI view) {
		this.view = view;
	}

	/**
	 * Set model
	 */
	public void setModel(GameState gameState) {
		this.gameState = gameState;
	}

	/**
	 * Interpret KeyEvent and handles by KeyCode
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		if (!this.gameState.isGameActive()) {
			return;
		}

		int keyCode = e.getKeyCode();
		if (keyCode == 27) { // 27 is key code for escape to pause

			if (this.view.isPaused()) {
				this.view.start(); // starts up again from a pause
			}

			else {
				this.view.stop(); // pauses game
			}

			return;
		}
		if (this.view.isPaused()) {
			return;
		}
		boolean isGameKey = true;
		switch (keyCode) {

		case 37: // (37 is key code for Left arrow)
			this.gameState.moveTetrominoLeft();
			break;
		case 40: // (40 is key code for down arrow)
			this.gameState.moveTetrominoDown();
			break;
		case 39: // (39 is key code for Right arrow)
			this.gameState.moveTetrominoRight();
			break;
		case 90: // (90 is key code for Z key rotate counter clockwise)
			this.gameState.rotateTetrominoLeft();
			break;
		case 65: // (65 is key code for C key rotate counter clockwise)
			this.gameState.rotateTetrominoRight();
			break;
		default: // (any other keys have no action)
			isGameKey = false;
			break;
		}
		if (isGameKey) {
			this.view.repaint();
		}
	}

	/**
	 * Detect a key release
	 */
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Detect a key press
	 */
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * Detect new game option click
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof javax.swing.JMenuItem) {
			newGameClicked();
		}

		if (e.getSource() == (this.view.getNetworkDialog()).hostButton) {
			hostClicked();
		}
		if (e.getSource() == (this.view.getNetworkDialog()).connectButton) {
			connectClicked();
		}
	}

	/**
	 * Start new single player game with timer
	 */
	private void newGameClicked() {
		this.view.stop();
		
		if(this.gameState.isGameActive()) {
			String yes = "Yes";
			Object[] choices = { yes, "No" };
			String gameRestart = (String) JOptionPane.showInputDialog(null, "Are you sure you want to start a new game?\n",
					"Warning!!", 1, null, choices, yes);
			if (gameRestart == null) {
				return;
			}
			else if (yes.equals(gameRestart)) {
				this.gameState.newSinglePlayerGame();
				this.view.resetTimer();
				this.view.start();
				return;

			}
			else {
				this.view.start();
				return;
			}
		}
		
		String single = "Single Player";
		Object[] players = { single, "Multiplayer" };
		String modalityOfGame = (String) JOptionPane.showInputDialog(null, "Please choose number of players:\n",
				"Players", 1, null, players, single);

		if (modalityOfGame == null) {
			return;
		}
		if (single.equals(modalityOfGame)) {
			this.gameState.newSinglePlayerGame();
			this.view.resetTimer();
			this.view.start();

		} else {

			String name = this.gameState.getLocalPlayerName();
			name = (name == null) ? "" : name;
			while ("".equals(name)) {
				name = JOptionPane.showInputDialog(null, "What's your name?", "Player Name", 1);
			}
			if (name == null) {
				return;
			}
			this.gameState.setLocalPlayerName(name);
			this.view.setModel(this.gameState);
			this.view.getNetworkDialog().setVisible(true);
		}

	}

	private void hostClicked() {
		try {
			if (this.hostingThread == null || (this.hostingThread != null && !this.hostingThread.isAlive())) {
				this.view.getNetworkDialog()
						.appendToLogTextArea("Public IP address: " + InetAddress.getLocalHost().getHostAddress().trim() + " Port: 4545\n");
				this.view.getNetworkDialog().appendToLogTextArea("Waiting for incoming connections...\n");

				ServerSocket serverSocket = new ServerSocket(
						Integer.parseInt((this.view.getNetworkDialog()).portTextField.getText()));
				Controller controller = this;
				this.hostingThread = new Thread(() -> {
					try {
						Socket socket = serverSocket.accept();
						NetworkAdapter networkAdapter = new NetworkAdapter(socket);
						networkAdapter.setServerSocket(serverSocket);
						Controller.this.gameState.setNetworkAdapter(networkAdapter);
						networkAdapter.setMessageListener(controller::messageReceived);
						networkAdapter.receiveMessagesAsync();

					} catch (IOException e) {

						Controller.this.view.getNetworkDialog()
								.appendToLogTextArea("Reading Socket Failed: " + e.getMessage() + "\n");
					}
				});

				this.hostingThread.start();
			} else if (this.hostingThread.isAlive()) {
				this.view.getNetworkDialog().appendToLogTextArea("Stopping Server Socket..\n");
				this.gameState.getNetworkAdapter().close();
				this.hostingThread = null;
				this.view.getNetworkDialog().appendToLogTextArea("Server Socket Stopped\n");
			}

		} catch (IOException e) {

			this.view.getNetworkDialog().appendToLogTextArea("Exception: " + e.getMessage());
		}
	}

	private void connectClicked() {
		try {
			Controller controller = this;

			try {
				String hostname = (this.view.getNetworkDialog()).hostnameTextField.getText();
				int port = Integer.parseInt((this.view.getNetworkDialog()).portTextField.getText());
				this.view.getNetworkDialog().appendToLogTextArea("Connecting..\n");
				Socket socket = new Socket(hostname, port);
				socket.setSoTimeout(15000);
				NetworkAdapter networkAdapter = new NetworkAdapter(socket);
				this.gameState.setNetworkAdapter(networkAdapter);
				networkAdapter.setMessageListener(controller::messageReceived);
				this.view.getNetworkDialog().appendToLogTextArea("Sucessfully Connected.\n");
				this.view.getNetworkDialog().appendToLogTextArea("Requesting new game.\n");
				networkAdapter.writeNew(this.gameState.getLocalPlayerName());
				networkAdapter.receiveMessagesAsync();
			} catch (IOException e) {

				this.view.getNetworkDialog()
						.appendToLogTextArea("Connection Failed. Try again..: " + e.getMessage() + "\n");
			}

		} catch (NumberFormatException e) {

			this.view.getNetworkDialog().appendToLogTextArea("Could not connect to network game: " + e.getMessage());
		}
	}

	private boolean newGamePrompt(String remotePlayerName) {
		Object[] players = { "Accept", "Reject" };
		String acceptNewGame = (String) JOptionPane.showInputDialog(null,
				"User " + remotePlayerName + " wants to start a new game, do you accept?\n", "New MultiplayerGame", 1,
				null, players, "Accept");

		return "Accept".equals(acceptNewGame);
	}

	private void startNewMultiplayerGame() {
		try {
			for (int i = 0; i < 3; i++) {
				this.view.getNetworkDialog().appendToLogTextArea("Starting new game in " + (3 - i) + " secs...\n");
				Thread.sleep(1000L);
			}
			this.view.getNetworkDialog().setVisible(false);
			this.gameState.newMultiPlayerGame();
			this.gameState.isConnectionActive(true);
			this.view.resetTimer();
			this.view.start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void messageReceived(NetworkAdapter.MessageType type, String s, int x, int y, int z, int[] others) {
		int i;
		TetrominoEnum[][] opponentsBoard;
		boolean newGame;
		NetworkAdapter networkAdapter = this.gameState.getNetworkAdapter();
		switch (type) {

		case NEW:
			this.gameState.setRemotePlayerName(s);
			newGame = newGamePrompt(s);
			networkAdapter.writeNewAck(this.gameState.getLocalPlayerName(), newGame ? 1 : 0);
			if (newGame) {

				this.view.getNetworkDialog().appendToLogTextArea("You accepted a new game request\n");
				startNewMultiplayerGame();

				break;
			}
			networkAdapter.close();
			this.view.getNetworkDialog().appendToLogTextArea("You rejected a new game request\n");
			this.view.getNetworkDialog().setVisible(false);
			break;

		case NEW_ACK:
			if (x == 1) {
				this.view.getNetworkDialog().appendToLogTextArea("User accepted your new game request\n");
				this.gameState.setRemotePlayerName(s);
				startNewMultiplayerGame();
			} else {
				this.view.getNetworkDialog().appendToLogTextArea("User rejected your new game request\n");
			}
		case STATUS:
			opponentsBoard = new TetrominoEnum[20][10];
			for (i = 0; i < others.length / 3; i++) {
				opponentsBoard[others[i * 3 + 1]][others[i * 3]] = Tetromino.TetrominoEnum
						.getEnumByValue(others[i * 3 + 2]);
			}
			this.gameState.setOpponentsBoard(opponentsBoard);
			this.gameState.setRemoteScore(x);
			this.gameState.isRemoteGameOver((y == 1));
			this.view.repaint();

			if (y == 1 && this.gameState.isGameOver()) {

				if (this.gameState.getRemoteScore() < this.gameState.getScore()) {
					this.view.setStatusBarText("You Win!!");
					this.gameState.isConnectionActive(false);
					break;
				}
				else if (this.gameState.getRemoteScore() == this.gameState.getScore()) {
					this.view.setStatusBarText("It's A Tie!");
					this.gameState.isConnectionActive(false);
					break;
				}
				else {
					this.view.setStatusBarText("You Lose!!");
					this.gameState.isConnectionActive(false);
					break;
				}
			}
			if (this.gameState.getRemoteScore() < this.gameState.getScore()) {
				this.view.setStatusBarText("You Are Winning!!");
				break;
			}
			else if(this.gameState.getRemoteScore() == this.gameState.getScore()) {
				this.view.setStatusBarText("You Are Tied!!");
				break;
			}
			else {
				this.view.setStatusBarText("You Are Losing!!");
				break;
			}
		case FILL:
			if (!this.gameState.isGameOver()) {
				this.gameState.fillBoard(x);
			}
			break;
		case QUIT:
			System.out.println("Quit Received");
			if (networkAdapter != null) {

				Socket socket = networkAdapter.socket();
				if (socket != null && !socket.isClosed()) {
					this.gameState.isConnectionActive(false);
					networkAdapter.writeQuit();
				}
				networkAdapter.close();
			}
			break;
		}
	}

	/**
	 * Detect window activation
	 */
	public void windowActivated(WindowEvent arg0) {
	}

	/**
	 * Detect window closing
	 */
	public void windowClosing(WindowEvent arg0) {
	}

	/**
	 * Detect window deactivation
	 */
	public void windowDeactivated(WindowEvent arg0) {
	}

	/**
	 * Detect window deiconification
	 */
	public void windowDeiconified(WindowEvent arg0) {
	}

	/**
	 * Detect window iconization
	 */
	public void windowIconified(WindowEvent arg0) {
	}

	/**
	 * Detect window opening
	 */
	public void windowOpened(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		NetworkAdapter networkAdapter = this.gameState.getNetworkAdapter();
		networkAdapter.writeQuit();
		// TODO Auto-generated method stub
	}

}