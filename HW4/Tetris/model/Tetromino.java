package Tetris.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**Abstract Tetromino class contains tetromino enumeration and similar shape manipulation methods 
 */
public abstract class Tetromino extends Object implements Iterable<Pair<Integer, Integer>> {

	protected short[][] coordinates;
    protected TetrominoEnum tetroEnum;

    /**Return TetrominoEnum type
     * @return TetrominoEnum by type
     */
    public TetrominoEnum getTetrominoType() {
        return this.tetroEnum;
    }

    /**Return width of TetrominoEnum shape space
     * @return shape space width
     */
    public int getWidth() {
        return this.coordinates[0].length;
    }

    /**Return height of TetrominoEnum shape space
     * @return shape space height
     */
    public int getHeight() {
        return this.coordinates.length;
    }

    /**Rotate TetronimoEnum coordinates clockwise
     */
    public void rotateRight() {
        short[][] rotatedCoordinates = new short[this.coordinates[0].length][this.coordinates.length];
        for (int y = 0; y < this.coordinates.length; y++) {
            for (int x = 0; x < this.coordinates[0].length; x++) {
                rotatedCoordinates[x][this.coordinates.length - 1 - y] = this.coordinates[y][x];
            }
        }
        this.coordinates = rotatedCoordinates;
        
    }

    /**Return TetronimoEnum coordinates counterclockwise
     */
    public void rotateLeft() {
        short[][] rotatedCoordinates = new short[this.coordinates[0].length][this.coordinates.length];
        for (int y = 0; y < this.coordinates.length; y++) {
            for (int x = 0; x < this.coordinates[0].length; x++) {
                rotatedCoordinates[this.coordinates[0].length - 1 - x][y] = this.coordinates[y][x];
            }
        }
        this.coordinates = rotatedCoordinates;
        
    }

    /**Iterator 
     */
    @Override
    public Iterator<Pair<Integer, Integer>> iterator() {
        List<Pair<Integer, Integer>> list = new LinkedList<>();

        for (int x = 0; x < this.coordinates[0].length; x++) {
            for (int y = 0; y < this.coordinates.length; y++) {

                short value = this.coordinates[y][x];
                if (value == 1) {
                    list.add(new Pair(x, y));
                }
            }
        }
        return list.iterator();
    }

    public enum TetrominoEnum {
        I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
        private int value;
        private static final Map<Integer, TetrominoEnum> reverseLookup;
        
        static {
            reverseLookup = new HashMap();
            for (TetrominoEnum tetromino : TetrominoEnum.values()) {
                reverseLookup.put(tetromino.getValue(), tetromino);
            }

        }

        TetrominoEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static TetrominoEnum getEnumByValue(int value) {
            TetrominoEnum tetromino = null;
            switch (value) {
            	case 0:
            		tetromino = (TetrominoEnum) reverseLookup.get(0);
            		break;
                case 1:
                    tetromino = (TetrominoEnum) reverseLookup.get(1);
                    break;
                case 2:
                    tetromino = (TetrominoEnum) reverseLookup.get(2);
                    break;
                case 3:
                    tetromino = (TetrominoEnum) reverseLookup.get(3);
                    break;
                case 4:
                    tetromino = (TetrominoEnum) reverseLookup.get(4);
                    break;
                case 5:
                    tetromino = (TetrominoEnum) reverseLookup.get(5);
                    break;
                case 6:
                    tetromino = (TetrominoEnum) reverseLookup.get(6);
                    break;
                default:
                    return tetromino;
            }
            return tetromino;
        }

        public static TetrominoEnum getRandomTetromino() {
            Random random = new Random();
            return Tetromino.TetrominoEnum.getEnumByValue(random.nextInt(7));
        }
    }
}