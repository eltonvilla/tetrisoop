package Tetris.model;

/**Pair class maintains a tetromino's x and y coordinates inhabiting its shape space
 */
public class Pair<X, Y> extends Object {

    private final X xValue;
    private final Y yValue;

    /**Initializes a Pair of x and y coordinate values
     */
    public Pair(X xValue, Y yValue) {
        this.xValue = xValue;
        this.yValue = yValue;
    }

    /**Get x value of coordinate
     */
    public X getX() {
        return (X) this.xValue;
    }

    /**Get y value of coordinate
     */
    public Y getY() {
        return (Y) this.yValue;
    }
}