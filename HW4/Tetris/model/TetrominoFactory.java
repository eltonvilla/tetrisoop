package Tetris.model;

public class TetrominoFactory {

    public TetrominoFactory() {
    }

    public Tetromino createTetromino(Tetromino.TetrominoEnum tetrominoEnum) {
        Tetromino tetromino = null;
        switch (tetrominoEnum) {
            case I:
                tetromino = new ITetromino();
                break;
            case J:
                tetromino = new JTetromino();
                break;
            case L:
                tetromino = new LTetromino();
                break;
            case O:
                tetromino = new OTetromino();
                break;
            case S:
                tetromino = new STetromino();
                break;
            case Z:
                tetromino = new ZTetromino();
                break;
            case T:
                tetromino = new TTetromino();
                break;
            default:
                return tetromino;
        }
        return tetromino;
    }

    public Tetromino getRandomTetromino() {
        return createTetromino(Tetromino.TetrominoEnum.getRandomTetromino());
    }
    
    private class ITetromino extends Tetromino {

        ITetromino() {
            short[][] coordinates = {{1, 1, 1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.I;
        }
    }

    private class JTetromino extends Tetromino {

        JTetromino() {
            short[][] coordinates = {{1, 0, 0}, {1, 1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.J;
        }
    }

    private class LTetromino extends Tetromino {

        LTetromino() {
            short[][] coordinates = {{0, 0, 1}, {1, 1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.L;
        }
    }

    private class OTetromino extends Tetromino {

        OTetromino() {
            short[][] coordinates = {{1, 1}, {1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.O;
        }
    }

    private class STetromino extends Tetromino {

        STetromino() {
            short[][] coordinates = {{0, 1, 1}, {1, 1, 0}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.S;
        }
    }

    private class ZTetromino extends Tetromino {

        ZTetromino() {
            short[][] coordinates = {{1, 1, 0}, {0, 1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.Z;
        }
    }

    private class TTetromino extends Tetromino {

        TTetromino() {
            short[][] coordinates = {{0, 1, 0}, {1, 1, 1}};
            this.coordinates = coordinates;
            tetroEnum = Tetromino.TetrominoEnum.T;
        }
    }
}