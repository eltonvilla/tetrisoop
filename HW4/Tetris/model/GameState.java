package Tetris.model;

import java.util.Iterator;
import java.util.Observable;
import java.util.Random;
import Tetris.model.Tetromino.TetrominoEnum;
import Tetris.model.NetworkAdapter;
import Tetris.model.Tetromino;

/**GameState keeps track of Tetromino shapes on board, score, level, and proper gameplay
 */
public class GameState extends Observable {

    private TetrominoFactory tf;
    private Tetromino.TetrominoEnum[][] board;
    private Tetromino.TetrominoEnum[][] opponentsBoard;
    private Tetromino tetromino;
    private short xPosition;
    private short yPosition;
    private int level;
    private Tetromino nextTetromino = null;

    private int score;
    private int remoteScore;
    private int lines;
    private int boardUsedSpace = 0;

    private boolean isGameActive;
    private boolean isGameOver;
    private boolean newScoreAvailable;
    private boolean levelUpgrade;
    private boolean advancingTetromino;
    private boolean isMultiplayerGame;
    private NetworkAdapter networkAdapter = null;
    private boolean isRemoteGameOver;
    private boolean isConnectionActive;
    private String localPlayerName;
    private String remotePlayerName;

    /**Begin a new game of Tetris on single player mode
     */
    public void newSinglePlayerGame() {
        newGame();
    }
    
    public void newMultiPlayerGame() {
        this.isMultiplayerGame = true;
        newGame();
    }
    
    public void setNetworkAdapter(NetworkAdapter networkAdapter) {
        this.networkAdapter = networkAdapter;
    }

    public NetworkAdapter getNetworkAdapter() {
        return this.networkAdapter;
    }

    /**Begin a new game of Tetris with initial conditions
     */
    private void newGame() {
        this.board = new TetrominoEnum[20][10];
        this.tf = new TetrominoFactory();
        this.nextTetromino = this.tf.getRandomTetromino();
        advanceNextTetromino();
        this.level = 1;
        this.score = 0;
        this.lines = 0;
        this.isGameActive = true;
        this.levelUpgrade = false;
        this.isGameOver = false;
    }
    
    /**Return whether a game is running or paused
     * @return true for an active game, false otherwise
     */
    public boolean isGameActive() {
        return this.isGameActive;
    }
    
    public boolean isConnectionActive() {
    	return this.isConnectionActive;
    }

    /**Return whether a game can no longer be played
     * @return true if a game over condition is met, false if it can still be played
     */
    public boolean isGameOver() {
        return this.isGameOver;
    }
    
    public boolean isRemoteGameOver() {
        return this.isRemoteGameOver;
    }
    
    public void isRemoteGameOver(boolean isRemoteGameOver) {
        this.isRemoteGameOver = isRemoteGameOver;
    }
    
    public void isConnectionActive(boolean status) {
    	this.isConnectionActive = status;
    }

    public boolean isMultiPlayerGame() {
        return this.isMultiplayerGame;
    }

    /**Return whether level needs to be upgraded
     * @return true if conditions for a level upgrade are met
     */
    public boolean levelUpgrade() {
        boolean temp = this.levelUpgrade;
        this.levelUpgrade = false;
        return temp;
    }
    
    /**Return the game's score
     * @return current score
     */
    public int getScore() {
        return this.score;
    }
    
    public int getRemoteScore() {
        return this.remoteScore;
    }
    
    public void setRemoteScore(int remoteScore) {
        this.remoteScore = remoteScore;
    }

    /**Return number of lines cleared in game
     * @return number of lines cleared
     */
    public int getLineCount() {
        return this.lines;
    }
    
    /**Return game's level
     * @return current game level
     */
    public int getLevel() {
        return this.level;
    }
    
    public void setLocalPlayerName(String localPlayerName) {
        this.localPlayerName = localPlayerName;
    }

    public void setRemotePlayerName(String remotePlayerName) {
        this.remotePlayerName = remotePlayerName;
    }

    public String getLocalPlayerName() {
        return this.localPlayerName;
    }

    public String getRemotePlayerName() {
        return this.remotePlayerName;
    }

    /**Return whether score needs to be updated to new score
     * @return true if new score available
     */
    public boolean newScoreAvailable() {
        boolean temp = this.newScoreAvailable;
        this.newScoreAvailable = false;
        return temp;
    }

    /**Get next Tetromino in sequence
     * @return next Tetromino
     */
    public Tetromino getNextTetromino() {
        return this.nextTetromino;
    }
    
    public void setOpponentsBoard(TetrominoEnum[][] opponentsBoard) {
        this.opponentsBoard = opponentsBoard;
    }

    public Tetromino.TetrominoEnum[][] getOpponentsBoard() {
        return this.opponentsBoard;
    }

    /**Advance tetromino by moving tetromino down the board
     */
    public void advanceTetromino() {
        this.advancingTetromino = true;
        moveTetrominoDown();
        this.advancingTetromino = false;
    }

    /**Move tetromino down the board one space
     */
    public void moveTetrominoDown() {
        this.yPosition = (short) (this.yPosition + 1);
        validateTetrominoPosition();
    }

    /**Move tetromino right one space if valid position
     */
    public void moveTetrominoRight() {
        this.xPosition = (short) (this.xPosition + 1);
        if (!validateTetrominoPosition()) {
            this.xPosition = (short) (this.xPosition - 1);
        }
    }

    /**Move tetromino left one space if valid position
     */
    public void moveTetrominoLeft() {
        this.xPosition = (short) (this.xPosition - 1);
        if (!validateTetrominoPosition()) {
            this.xPosition = (short) (this.xPosition + 1);
        }
    }

    /**Rotate tetromino clockwise if valid position
     */
    public void rotateTetrominoRight() {
        this.tetromino.rotateRight();
        if (!validateTetrominoPosition()) {
            this.tetromino.rotateLeft();
        }
    }

    /**Rotate tetromino left if valid position
     */
    public void rotateTetrominoLeft() {
        this.tetromino.rotateLeft();
        if (!validateTetrominoPosition()) {
            this.tetromino.rotateRight();
        }
    }

    /**Get a board of TetrominoEnums and their configurations on the board
     * @return TetrominoEnum 2D array
     */
    public Tetromino.TetrominoEnum[][] getBoard() {
        TetrominoEnum[][] boardCopy = new TetrominoEnum[this.board.length][];
        for (int y = 0; y < this.board.length; y++) {
            boardCopy[y] = (TetrominoEnum[]) this.board[y].clone();
        }
        Tetromino.TetrominoEnum type = this.tetromino.getTetrominoType();
        Iterator<Pair<Integer, Integer>> iter = this.tetromino.iterator();
        while (iter.hasNext()) {

            Pair<Integer, Integer> pair = (Pair) iter.next();
            boardCopy[this.yPosition + (pair.getY())][this.xPosition + (pair.getX())] = type;
        }

        return boardCopy;
    }

    /**Check for an overlap of tetrominos in the current space
     * @return true if space is already taken up by a teromino and would cause an overlap
     */
    private boolean doesOverlapCurrent(Pair<Integer, Integer> pair) {
        return (this.board[this.yPosition + (pair.getY())][this.xPosition + (pair.getX())] != null);
    }

    /**Check for an overlap of tetrominos in the next space below
     * @return true if there is no more space to move below in the board
     */
    private boolean doesOverlapNext(Pair<Integer, Integer> pair) {
        return !(this.yPosition + (pair.getY()) + 1 <= 19 && this.board[this.yPosition + (pair.getY()) + 1][this.xPosition + (pair.getX())] == null);
    }

    /**Validate tetromino position by checking for collisions and out of bounds
     * @return true if position is valid for tetromino
     */
    private boolean validateTetrominoPosition() {
        if (this.xPosition < 0 || this.xPosition + this.tetromino.getWidth() - 1 > 9) {
            return false;
        }

        if (this.yPosition < 0 || this.yPosition + this.tetromino.getHeight() - 1 > 19) {
            return false;
        }
        Iterator<Pair<Integer, Integer>> iter = this.tetromino.iterator();
        while (iter.hasNext()) {

            Pair<Integer, Integer> pair = (Pair) iter.next();

            if (doesOverlapCurrent(pair)) {
                return false;
            }

            if (doesOverlapNext(pair)) {

                if (this.yPosition <= 1) {

                    gamerOver();
                    return false;
                }

                incorporateTetromino();
                checkForCompletedLines();

                advanceNextTetromino();
                
                if (!validateTetrominoPosition()) {
                    if (this.isMultiplayerGame) {
                        sendBoardOverNetwork();
                    }
                	
                    return false;
                }
                setChanged();
                notifyObservers();
                return true;
            }
        }
        if (this.isMultiplayerGame) {
            sendBoardOverNetwork();
        }

        return true;
    }

    /**End game of Tetris by creating a Game Over
     */
    private void gamerOver() {
        this.isGameActive = false;
        this.isGameOver = true;
        if (this.isMultiplayerGame) {

            sendBoardOverNetwork();
            if (this.isRemoteGameOver) {

                this.networkAdapter.writeQuit();
                this.isConnectionActive(false);
                this.networkAdapter.close();
            }
        }

        setChanged();
        notifyObservers();
    }

    /**Set tetromino piece down on the board for possible clearing
     */
    private void incorporateTetromino() {
        Tetromino.TetrominoEnum type = this.tetromino.getTetrominoType();
        Iterator<Pair<Integer, Integer>> iter = this.tetromino.iterator();
        while (iter.hasNext()) {

            Pair<Integer, Integer> pair = (Pair) iter.next();

            this.board[this.yPosition + (pair.getY())][this.xPosition + (pair.getX())] = type;
        }

        this.newScoreAvailable = true;
        this.score += 5;
    }

    /**Get the next random tetromino piece
     */
    private void advanceNextTetromino() {
        this.tetromino = this.nextTetromino;
        this.nextTetromino = this.tf.getRandomTetromino();
        this.xPosition = 4;
        this.yPosition = 0;
        this.boardUsedSpace += 4;
    }

    /**Check the board for horizontal lines completely filled by tetromino shapes
     */
    private void checkForCompletedLines() {
        int completedLinesAtOnce = 0;
        for (int y = 0; y < 20; y++) {

            boolean completedLine = true;
            for (int x = 0; x < 10; x++) {
                if (this.board[y][x] == null) {

                    completedLine = false;
                    break;
                }
            }
            if (completedLine) {

                removeLine(y);

                this.lines++;
                if (this.lines % 5 == 0) {

                    this.level++;
                    this.levelUpgrade = true;
                }
                completedLinesAtOnce++;
                this.newScoreAvailable = true;

                y--;
            }
        }
        switch (completedLinesAtOnce) {

            case 1:
                this.score += 100;
                break;
            case 2:
                this.score += 300;
                if (this.isMultiplayerGame) {
                    this.networkAdapter.writeFill(1);
                }
                break;
            case 3:
                this.score += 600;
                if (this.isMultiplayerGame) {
                    this.networkAdapter.writeFill(2);
                }
                break;
            case 4:
                this.score += 1000;
                if (this.isMultiplayerGame) {
                    this.networkAdapter.writeFill(4);
                }
                break;
        }
    }
    
    
    public void fillBoard(int additionalLines) {
        Random random = new Random();
        for (int i = 0; i < additionalLines; i++) {

            for (int y = 1; y < 20; y++) {
                this.board[y - 1] = this.board[y];
            }
            this.board[19] = new Tetromino.TetrominoEnum[10];
            for (int x = 0; x < 10; x++) {
                this.board[19][x] = Tetromino.TetrominoEnum.FILLER;
            }
            this.board[19][random.nextInt(10)] = null;
            this.boardUsedSpace += 9;
        }
    }

    public void sendBoardOverNetwork() {
        Tetromino.TetrominoEnum[][] arrayOfTetrominoEnum = getBoard();
        int[] boardCoordinates = new int[this.boardUsedSpace * 3];
        int counter = 0;
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 10; x++) {
                if (arrayOfTetrominoEnum[y][x] != null) {

                    boardCoordinates[counter * 3] = x;
                    boardCoordinates[counter * 3 + 1] = y;
                    boardCoordinates[counter * 3 + 2] = arrayOfTetrominoEnum[y][x].getValue();
                    counter++;
                }
            }
        }
        this.networkAdapter.writeStatus(this.score, this.isGameOver ? 1 : 0, boardCoordinates);
    }
    
    

    /**Rid a cleared line on the board of tetromino shapes and move everything above it down one space
     */
    private void removeLine(int yLine) {
        for (int y = yLine; y > 0; y--) {
            this.board[y] = this.board[y - 1];
        }

        this.board[0] = new Tetromino.TetrominoEnum[10];
        this.boardUsedSpace -= 10;
    }
    
}