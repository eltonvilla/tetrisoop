package Tetris.animation;

import javax.swing.Timer;

public class AnimationApplet extends NoApplet {

    private static final long serialVersionUID = 1L;
    protected Timer timer = null;

    protected int delay;

    public AnimationApplet(String[] args) {
        super(args);
    }

    @Override
    public void init() {
        super.init();
        this.timer = new Timer(this.delay, e -> periodicTask());
    }

    @Override
    public void start() {
        this.timer.start();
    }

    @Override
    public void stop() {
        this.timer.stop();
    }

    public void periodicTask() {
        repaint();
    }
}