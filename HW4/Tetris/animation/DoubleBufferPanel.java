package Tetris.animation;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public abstract class DoubleBufferPanel
        extends JPanel {

    private static final long serialVersionUID = 1L;
    private Image image;
    private Graphics offScreen;
    protected Dimension dim;
    protected static final int DEFAULT_WIDTH = 550;
    protected static final int DEFAULT_HEIGHT = 650;

    public DoubleBufferPanel() {
        this.dim = new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DoubleBufferPanel(Dimension dim) {
        this.dim = dim;
    }

    public void init() {
        this.image = new BufferedImage(this.dim.width, this.dim.height, BufferedImage.TYPE_INT_RGB);
        this.offScreen = this.image.getGraphics();
    }

    @Override
    public final void update(Graphics g) {
        paintFrame(this.offScreen);
        g.drawImage(this.image, 0, 0, this);
    }

    @Override
    public final void paint(Graphics g) {
        update(g);
    }

    protected abstract void paintFrame(Graphics paramGraphics);
}